%global srcname novaclient-os-networks
%global upstreamname os_networksv2_python_novaclient_ext

Name:		python-%{srcname}
Version:	0.25
Release:	2%{?dist}
Summary:	Adds network extension support to python-novaclient

Group:		Development/Libraries
License:	ASL 2.0
URL:		http://pypi.python.org/pypi/%{upstreamname}
Source0:	http://pypi.python.org/packages/source/o/%{upstreamname}/%{upstreamname}-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	python2-devel

%description
%{summary}
%package -n python2-%{srcname}
Summary:	%{summary}
BuildRequires:	python-novaclient
Requires:	python-novaclient
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
%{summary}

%prep
%autosetup -n %{upstreamname}-%{version}


%build
%py2_build

%install
%py2_install

%check
%{__python2} setup.py test

%files -n python2-%{srcname}
%doc README.rst
%{python2_sitelib}/*

%changelog
* Thu Mar 03 2016 Ricardo Cordeiro <gryfrev8-redhat.com-rjmco@tux.com.pt> - 0.25-2
- Replaced the use of sum with summary

* Sat Feb 20 2016 Ricardo Cordeiro <gryfrev8-redhat.com-rjmco@tux.com.pt> - 0.25-1
- Initial package
